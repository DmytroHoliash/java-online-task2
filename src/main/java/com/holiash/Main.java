package com.holiash;

import java.util.Scanner;

/**
 * Main class
 * @author Dmytro Holiash
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input number of guests(>2): ");
        int guestsNumber = scanner.nextInt();
        while (guestsNumber < 3) {
            System.out.println("Number of guests must be bigger than 2\nInput again:");
            guestsNumber = scanner.nextInt();
        }
        RumorProbabilityEstimator rps = new RumorProbabilityEstimator(guestsNumber);
        rps.estimateProbability();
        rps.show();
    }
}
