package com.holiash;

/**
 * RumorProbabilityEstimator class calculating
 * probability that everyone on the party of N
 * people will hear a rumor about Alice
 * @author Dmytro Holiash
 * @version 1.0
 */
class RumorProbabilityEstimator {
    /**
     * Number of attempts
     */
    private static final int ATTEMPTS = 1000;
    /**
     * Number of guests at the party
     */
    private int n;
    /**
     * Boolean array in which
     * TRUE means that guest already heard the rumor
     * and vice versa
     */
    private boolean[] guests;
    /**
     * Number of successes
     */
    private int successfulAttempts;
    /**
     * Probability that everyone at the party will
     * hear the rumor about Alice
     */
    private double probability;
    /**
     * Average number of people who heard the rumor
     */
    private int averageNumberOfPeopleWhoHeard;

    /**
     * Creates the party with specified number of guests
     * @param guestsNumber specifies number of guests
     *                     at the party
     */
    RumorProbabilityEstimator(int guestsNumber) {
        this.n = guestsNumber;
        this.guests = new boolean[guestsNumber];
    }

    /**
     * Calculate probability that everyone at the party
     * will hear the rumor abot Alice
     */
    void estimateProbability() {
        int currentGuest;
        int nextGuest;
        boolean alreadyHeard;
        int numberOfPeopleWhoHeard = 0;
        for (int i = 0; i < ATTEMPTS; i++) {
            this.guests[0] = true;
            currentGuest = 0;
            while (true) {
                nextGuest = this.selectNextGuest(currentGuest);
                alreadyHeard = this.guests[nextGuest];
                if (alreadyHeard) break;
                this.guests[nextGuest] = true;
                numberOfPeopleWhoHeard++;
                currentGuest = nextGuest;
                if (this.isEveryoneHeard()) {
                    this.successfulAttempts++;
                    break;
                }
            }
            this.guests = new boolean[this.n];
        }
        this.probability = (double) this.successfulAttempts / ATTEMPTS;
        this.averageNumberOfPeopleWhoHeard = numberOfPeopleWhoHeard / ATTEMPTS;
    }

    /**
     * Outputs the result of calculating probability to the console
     */
    void show() {
        System.out.printf("Number of attempts = %d\n", ATTEMPTS);
        System.out.printf("The probability that everyone will hear the rumor = %.3f\n", this.probability);
        System.out.printf("Average number of people who heard = %d\n", this.averageNumberOfPeopleWhoHeard);
    }

    /**
     * Randomising next guest until he is different
     * from current guest
     * @param currentGuest specifies the current guest
     * @return next guest
     */
    private int selectNextGuest(int currentGuest) {
        int nextGuest = (int) (Math.random() * (n));
        while (nextGuest == currentGuest) {
            nextGuest = (int) (Math.random() * (n));
        }
        return nextGuest;
    }

    /**
     * Check whether everyone heard the rumor
     * @return TRUE if everyone heard the rumor and vice versa
     */
    private boolean isEveryoneHeard() {
        for (boolean guest : this.guests) {
            if (!guest) return false;
        }
        return true;
    }
}
